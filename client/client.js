let ws = new WebSocket("ws://localhost:8080");
ws.addEventListener("open", () => {
    console.log("We are connected");
});

ws.addEventListener('message', function (event) {
    var messages = JSON.parse(event.data).map(msg => msg).join("<br />")
    document.getElementById('server_message').innerHTML = messages;
});

ws.addEventListener("close", function () {
    alert('Web Socket server is down! Pleas come back later.')
    location.href = '../index.html'
})

document.getElementById('testbtn').addEventListener('click', function (event) {
    if (ws.readyState == ws.CLOSED) {
        alert('Web Socket server is down! Pleas come back later.')
    }
    if (ws.readyState == ws.OPEN) {
        ws.send(document.getElementById('client_message').innerHTML);
    }
})