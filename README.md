# Basic Web Socket server/client

## 1. Installation

```
$ npm install
```

## 2. Running

### 2.1. Start up the Server

```
$ npm run server
```

### 2.2. Test the Clients

```
$ open index.html
```