// Importing the required modules
const WebSocketServer = require('ws');

// Creating a new websocket server
const wss = new WebSocketServer.Server({ port: 8080 })

// dummy database of messages
var MESSAGES = []

// Creating connection using websocket
wss.on("connection", ws => {
    console.log("new client connected");
    ws.send(JSON.stringify(MESSAGES))
    // sending message
    ws.on("message", data => {
        console.log(`Client has sent us: ${data}`)
        MESSAGES.push(data + "")
        wss.clients.forEach(function each(client) {
            if (client.readyState === ws.OPEN) {
                client.send(JSON.stringify(MESSAGES));
            }
        });
    });
    // handling what to do when clients disconnects from server
    ws.on("close", () => {
        console.log("the client has disconnected");
    });
    // handling client connection error
    ws.onerror = function () {
        console.log("Some Error occurred")
    }
});
console.log("The WebSocket server is running on port 8080");